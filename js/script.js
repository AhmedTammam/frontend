// some scripts
function ShowItinerary(divid) {
	if ($('#' + divid).hasClass('active')) {
		// console.log($('#' + divid).hasClass("active"));
		$($('#' + divid)).find('.item-details').hide();
		$('#overlay').fadeOut(500);
	} else {
		$($('#' + divid)).find('.item-details').show();
		$('#overlay').fadeIn(500);
	}
	scrollToDiv(divid);
}

function scrollToDiv(divid) {
	$('#' + divid).toggleClass('active');
	$('html, body').animate({ scrollTop: $('#' + divid).offset().top }, 500);
}

////////////////// booking steps function
function activate_block(id) {
	$('#' + id).removeClass('panel-active').addClass('panel-disable').children('.full-view').slideUp();
	$('#' + id).children('.compact-view').slideDown().addClass('done');

	$('#' + id)
		.next('.panel-booking')
		.addClass('panel-active')
		.removeClass('panel-disable')
		.children('.compact-view')
		.hide();
	$('#' + id).next('.panel-booking').children('.full-view').slideDown();
	//$('#wrap_traveller_active').slideDown().closest('.panel').removeClass('panel-disable').addClass('panel-active');
}

////////////// hide loading when page loads finish
$(window).load(function() {
	// $('.loading-content').fadeOut(); // remove comment
});

// jquery ready start
$(document).ready(function() {
	// jQuery code

	$('#accordion_faq .info-wrap').hide();

	$('#accordion_faq .title a').click(function(e) {
		e.preventDefault();
		if ($('#accordion_faq .info-wrap').is(':visible')) {
			$('#accordion_faq .info-wrap').slideUp(300);
			$('.item-faq').removeClass('active');
		}
		if ($(this).closest('.item-faq').find('.info-wrap').is(':visible')) {
			$(this).closest('.item-faq').find('.info-wrap').slideUp(300);
			$(this).closest('.item-faq').removeClass('active');
		} else {
			$(this).closest('.item-faq').find('.info-wrap').slideDown(300);
			$(this).closest('.item-faq').addClass('active');
			// $(this).children(".plusminus").text('-');
		}
	});

	////////////////////// custom scroll
	if ($('.custom-scroll').length > 0) {
		// check if element exists
		$('.custom-scroll').mCustomScrollbar({
			theme: 'minimal-dark'
		});
	}

	////////////////////// subscribe popup
	if ($('.widget-subscribe').length > 0) {
		// check if element exists
		setTimeout(function() {
			$('.widget-subscribe').show().addClass('open');
		}, 3000); // 3 seconds.
		setTimeout(function() {
			$('.widget-subscribe').addClass('shake');
		}, 4500); // 4.5 seconds.

		//close
		$('.widget-subscribe .close').click(function() {
			$('.widget-subscribe').hide('slide', { direction: 'right' }, 1000);
		});
	}

	/////////////////////// restyle file input
	$(':file').change(function() {
		file_val = $(this).val().replace(/.*(\/|\\)/, '');
		$(this).closest('.file-input-wrap').find('.filename-input').text(file_val);
	});
	$('.del-file-input').click(function(e) {
		e.preventDefault();
		$(this).closest('.file-input-wrap').find('[type=file]').val('');
		$(this).closest('.file-input-wrap').find('.filename-input').text('');
	});

	/////////////////////// popover /plugins/popover
	if ($('a.uipopover').length > 0) {
		// check if element exists
		$('a.uipopover').webuiPopover({ title: 'Choose seat', width: 300, animation: 'pop', closeable: true });
	}

	///////////////// CLOSE EMAIL NOTIFY flight
	$('.btn-price-alert').click(function() {
		$('.panel-notify').slideToggle();
		$('#overlay').fadeIn(500);
	});

	$('.alert-dismiss .close').click(function(e) {
		e.preventDefault();
		$(this).closest('.alert-dismiss').slideUp();
	});

	$('.alert-hide .close').click(function(e) {
		e.preventDefault();
		$(this).closest('.alert-hide').fadeOut();
	});

	/////////////////////// sticky /plugins/sticky
	if ($('#sticker').length > 0) {
		// check if element exists
		if ($(window).width() > 768) {
			$('#sticker').stick_in_parent({
				offset_top: 60,
				// container: $(".section-content")
				parent: $('.section-content')
			});
		}
	}

	/////////////////////// simple tab
	if ($('.jq-tab').length > 0) {
		// check if element exists
		btn = $('.jq-tab .nav li');
		btn.click(function(e) {
			e.preventDefault();
			var index = $(this).index();
			$(this).closest('.jq-tab').find('.tab-item').hide();
			$(this).closest('.jq-tab').find('.tab-item').eq(index).show();
			$(this).siblings('li').removeClass('active');
			$(this).addClass('active');
		});
	}

	/////////////////////// simple jquery number counter
	$('.count').each(function() {
		$(this).prop('Counter', 10).animate(
			{
				Counter: $(this).text()
			},
			{
				duration: 3000,
				easing: 'swing',
				step: function(now) {
					$(this).text(Math.ceil(now));
				}
			}
		);
	});

	//////////////////////// Bootstrap number spinner
	$('.number-spinner').click(function(e) {
		e.preventDefault();

		var fieldName = $(this).attr('data-field');
		var type = $(this).attr('data-type');
		var input = $("input[name='" + fieldName + "']");
		var currentVal = parseInt(input.val());
		if (!isNaN(currentVal)) {
			if (type == 'minus') {
				var minValue = parseInt(input.attr('min'));
				if (!minValue) minValue = 0;
				if (currentVal > minValue) {
					input.val(currentVal - 1).change();
				}
				if (parseInt(input.val()) == minValue) {
					$(this).attr('disabled', true);
				}
			} else if (type == 'plus') {
				var maxValue = parseInt(input.attr('max'));
				if (!maxValue) maxValue = 999;
				if (currentVal < maxValue) {
					input.val(currentVal + 1).change();
				}
				if (parseInt(input.val()) == maxValue) {
					$(this).attr('disabled', true);
				}
			}
		} else {
			input.val(0);
		}
	});

	$('.spinner-value').focusin(function() {
		$(this).data('oldValue', $(this).val());
	});
	$('.spinner-value').change(function() {
		var minValue = parseInt($(this).attr('min'));
		var maxValue = parseInt($(this).attr('max'));
		if (!minValue) minValue = 0;
		if (!maxValue) maxValue = 999;
		var valueCurrent = parseInt($(this).val());

		var name = $(this).attr('name');
		if (valueCurrent >= minValue) {
			$(".number-spinner[data-type='minus'][data-field='" + name + "']").removeAttr('disabled');
		} else {
			$(this).val($(this).data('oldValue'));
		}
		if (valueCurrent <= maxValue) {
			$(".number-spinner[data-type='plus'][data-field='" + name + "']").removeAttr('disabled');
		} else {
			alert('Sorry, the maximum value was reached');
			$(this).val($(this).data('oldValue'));
		}
	});
	$('.spinner-value').keydown(function(e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if (
			$.inArray(e.keyCode, [ 46, 8, 9, 27, 13, 190 ]) !== -1 ||
			// Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)
		) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	//////////////////////// Unitegallery. plugins/unitegallery
	if ($('#hotel_gallery').length > 0) {
		// check if element exists
		jQuery('#hotel_gallery').unitegallery({
			theme_enable_text_panel: false,
			gallery_autoplay: true,
			slider_enable_progress_indicator: false,
			slider_enable_play_button: false,
			thumb_round_corners_radius: 5,
			strippanel_handle_align: 'bottom',
			strippanel_background_color: '#fff',
			gallery_height: 600,
			theme_enable_hidepanel_button: false,
			thumb_selected_border_color: '#EC6F23',
			strippanel_padding_buttons: 7
		});
	}

	//////////////////////// on login page activate tab from outside
	$('.target-tab').click(function(e) {
		e.preventDefault();
		$('a[href="' + $(this).attr('href') + '"]').tab('show');
	});

	///////////////// Owl carousel
	if ($('.owl-carousel').length > 0) {
		// check if element exists
		$('.owl-carousel').owlCarousel({
			loop: true,
			margin: 15,
			nav: true,
			navText: [ "<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>" ],
			//items: 4,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 3
				},
				1000: {
					items: 4
				}
			}
		});
	}

	///////////////// Owl carousel
	if ($('.owl-carousel-three').length > 0) {
		// check if element exists
		$('.owl-carousel-three').owlCarousel({
			loop: true,
			margin: 15,
			nav: true,
			navText: [ "<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>" ],
			//items: 3,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 2
				},
				1000: {
					items: 3
				}
			}
		});
	}

	///////////////// more tickets
	$('.btn-ticket-more').click(function() {
		if ($(this).prev('.tickets-more-wrap').is(':visible')) {
			$(this).text('+ More options at same price');
		} else {
			$(this).text('- Less options');
		}
		$(this).prev('.tickets-more-wrap').slideToggle();
	});

	///////////////// fixed menu on scroll
	if ($(window).scrollTop() > 15) {
		$('.header-nav').addClass('fixed-top');
	}
	$(window).scroll(function() {
		if ($(window).width() > 1024) {
			if ($(this).scrollTop() > 15) {
				$('.header-nav').addClass('fixed-top');
			} else {
				$('.header-nav').removeClass('fixed-top');
			}
		}
	});

	/////////////// widget show hide
	$('.btn-widget-contact').click(function(e) {
		e.preventDefault();
		if ($('.widget-contact').hasClass('active') == false) {
			$('.widget-contact').addClass('active').removeClass('hidewidget');
			$('.btn-widget-contact').html(' <i class="fa fa-times"></i> Close  ');
		} else {
			$('.widget-contact').removeClass('active').addClass('hidewidget');
			$('.btn-widget-contact').html(' <i class="fa fa-phone"></i> Contact  ');
		}
		//$(".widget-contact").toggleClass('active');
	});

	/////////////// widget show hide
	$('.btn-widget-feedback').click(function(e) {
		e.preventDefault();
		if ($('.widget-feedback').hasClass('active') == false) {
			$('.widget-feedback').addClass('active').removeClass('hidewidget2');
			$('.btn-widget-feedback').html(' <i class="fa fa-close"></i> Close  ');
		} else {
			$('.widget-feedback').removeClass('active').addClass('hidewidget2');
			$('.btn-widget-feedback').html(' <i class="fa fa-envelope"></i> Feedback ');
		}
		// $(".widget-feedback").toggleClass('active');
	});

	////////////////// booking steps
	$('.compact-view').click(function() {
		$('.full-view').hide();
		$('.compact-view.done').show();
		$('.panel-booking').removeClass('panel-active').addClass('panel-disable');
		$(this).parent('.panel-booking').removeClass('panel-disable').addClass('panel-active');
		$(this).siblings('.full-view').slideDown();
		$(this).slideUp();
	});

	////////////////// tooltip moves with cursor
	$('[data-tooltip]')
		.hover(
			function(event) {
				//alert('salam');
				var toolTip = $(this).attr('data-tooltip');
				$('<span class="mytip"></span>')
					.text(toolTip)
					.appendTo('body')
					.css('top', event.pageY - 10 + 'px')
					.css('left', event.pageX + 20 + 'px')
					.fadeIn('slow');
			},
			function() {
				$('.mytip').remove();
			}
		)
		.mousemove(function(event) {
			$('.mytip').css('top', event.pageY - 10 + 'px').css('left', event.pageX + 20 + 'px');
		});

	////////////////// collabse link change text when collapsed
	$('.panel-booking a[data-toggle=collapse]').click(function() {
		if ($(this).attr('aria-expanded') == 'false') {
			$(this).html('Hide options <i class="fa fa-chevron-up"></i>');
		} else {
			$(this).html('More options <i class="fa fa-chevron-down"></i>');
		}
	});

	$('a.btn-coupon').click(function() {
		if ($(this).attr('aria-expanded') == 'false') {
			$(this).html('Hide this <i class="fa fa-chevron-up"></i>');
		} else {
			$(this).html('I have coupon <i class="fa fa-chevron-down"></i>');
		}
	});

	/////////////////// on top of page appears flight  edit form
	$('.section-modify').hide();
	$('.btn-modify').click(function() {
		$('.section-modify').slideToggle();
	});

	// overlay click deactive ticket
	$('#overlay').on('click', function() {
		$('.item-result-wrap.active').find('.btn-details').click();
		if ($(this).hasClass('mobile-overlay')) {
			$('.btn-filter-close').click();
		}
	});

	////////////////////// SHow filter on mobile
	$('.btn-filter').click(function() {
		$('#filter-area').show();
		$('#overlay').show().addClass('mobile-overlay');
		scrollToDiv('filter-area');
	});

	$('.btn-filter-close').click(function() {
		$('#filter-area').hide();
		$('#overlay').hide();
	});

	//////////////////////// Price range jQuery UI
	// $("#price-range").slider({
	//      range: true,
	//      min: 0,
	//      max: 500,
	//      values: [ 75, 300 ],
	//      slide: function( event, ui ) {
	//        $( "#price-min" ).val('AED ' + ui.values[ 0 ]);
	//        $( "#price-max" ).val('AED ' + ui.values[ 1 ] );
	//      }
	//    });
	//    $( "#price-min" ).val('AED ' + $( "#price-range" ).slider( "values", 0 ));
	//    $( "#price-max" ).val('AED ' + $( "#price-range" ).slider( "values", 1 ));

	//////////////////////// Departure range jQuery UI
	$('#departure-range').slider({
		range: true,
		min: 0,
		max: 1440,
		step: 5,
		values: [ 360, 1200 ],
		slide: function(event, ui) {
			$('#depart-min').val(ui.values[0]);
			$('#depart-max').val(ui.values[1]);
		}
	});
	$('#depart-min').val($('#departure-range').slider('values', 0));
	$('#depart-max').val($('#departure-range').slider('values', 1));

	//////////////////////// Arrive range jQuery UI
	$('#arrive-range').slider({
		range: true,
		min: 0,
		max: 1440,
		step: 5,
		values: [ 360, 1200 ],
		slide: function(event, ui) {
			$('#arrive-min').val(ui.values[0]);
			$('#arrive-max').val(ui.values[1]);
		}
	});
	$('#arrive-min').val($('#arrive-range').slider('values', 0));
	$('#arrive-max').val($('#arrive-range').slider('values', 1));

	//////////////////////// Duration range jQuery UI
	$('#duration-range').slider({
		range: true,
		min: 0,
		max: 1440,
		step: 5,
		values: [ 0, 1430 ],
		slide: function(event, ui) {
			$('#duration-min').val(ui.values[0]);
			$('#duration-max').val(ui.values[1]);
		}
	});
	$('#duration-min').val($('#duration-range').slider('values', 0));
	$('#duration-max').val($('#duration-range').slider('values', 1));

	//////////////////////// Bootstrap tooltip
	if ($("[data-toggle='tooltip']").length > 0) {
		// check if element exists
		$("[data-toggle='tooltip']").tooltip();
	}

	///////////////// AUTOCOMPLATE
	var demo_listing = [
		{
			label: 'Dublin, Ireland',
			label_name: 'Dublin, Ireland',
			value: 'Dublin, Ireland',
			label_code: '(DUB)',
			category: 'Location'
		},
		{
			label: 'Dubuque - IA, United States',
			label_name: 'Dubuque - IA, United States',
			value: 'Dubuque - IA, United States',
			label_code: '(DBQ)',
			category: 'Location'
		},
		{
			label: 'Dubai, United Arab Emirates',
			label_name: 'Dubai, United Arab Emirates',
			value: 'Dubai, United Arab Emirates',
			label_code: '(DXB)',
			category: 'Location'
		},
		{
			label: 'DuBois - PA, United States',
			label_name: 'DuBois - PA, United States',
			value: 'DuBois - PA, United States',
			label_code: '(DUJ)',
			category: 'Location'
		},
		{
			label: 'Dublin - OH, United States',
			label_name: 'Dublin - OH, United States',
			value: 'Dublin - OH, United States',
			label_code: '(DU4)',
			category: 'Location'
		},
		{
			label: 'Dubrovnik-South Dalmatia, Croatia',
			label_name: 'Dubrovnik-South Dalmatia, Croatia',
			value: 'Dubrovnik-South Dalmatia, Croatia',
			label_code: '(DBV)',
			category: 'Location'
		},
		{
			label: 'Mespil',
			label_name: 'Mespil, DUBLIN, IE',
			value: 'Mespil, DUBLIN, IE',
			label_code: '(DUB)',
			category: 'Hotel'
		},
		{
			label: 'Temple Bar Hotel',
			label_name: 'Temple Bar Hotel, DUBLIN, IE',
			value: 'Temple Bar Hotel, DUBLIN, IE',
			label_code: '(DUB)',
			category: 'Hotel'
		},
		{
			label: 'Morrison',
			label_name: 'Morrison, DUBLIN, IE',
			value: 'Morrison, DUBLIN, IE',
			label_code: '(DUB)',
			category: 'Hotel'
		},
		{
			label: 'Avari Dubai',
			label_name: 'Avari Dubai, DUBAI, AE',
			value: 'Avari Dubai, DUBAI, AE',
			label_code: '(DXB)',
			category: 'Hotel'
		},
		{
			label: 'Dubai Grand',
			label_name: 'Dubai Grand, DUBAI, AE',
			value: 'Dubai Grand, DUBAI, AE',
			label_code: '(DXB)',
			category: 'Hotel'
		},
		{
			label: 'Four Points Sheraton Bur Dubai',
			label_name: 'Four Points Sheraton Bur Dubai, DUBAI, AE',
			value: 'Four Points Sheraton Bur Dubai, DUBAI, AE',
			label_code: '(DXB)',
			category: 'Hotel'
		},
		{
			label: 'Riu Plaza The Gresham Dublin',
			label_name: 'Riu Plaza The Gresham Dublin, DUBLIN, IE',
			value: 'Riu Plaza The Gresham Dublin, DUBLIN, IE',
			label_code: '(DUB)',
			category: 'Hotel'
		},
		{
			label: 'Le Meridien Dubai',
			label_name: 'Le Meridien Dubai, DUBAI, AE',
			value: 'Le Meridien Dubai, DUBAI, AE',
			label_code: '(DXB)',
			category: 'Hotel'
		},
		{
			label: 'The Apartments Dubai World Trade Centre',
			label_name: 'The Apartments Dubai World Trade Centre, DUBAI, AE',
			value: 'The Apartments Dubai World Trade Centre, DUBAI, AE',
			label_code: '(DXB)',
			category: 'Hotel'
		},
		{
			label: 'Grand Hyatt Dubai',
			label_name: 'Grand Hyatt Dubai, DUBAI, AE',
			value: 'Grand Hyatt Dubai, DUBAI, AE',
			label_code: '(DXB)',
			category: 'Hotel'
		},
		{
			label: 'Clayton Hotel Burlington Road.',
			label_name: 'Clayton Hotel Burlington Road., DUBLIN, IE',
			value: 'Clayton Hotel Burlington Road., DUBLIN, IE',
			label_code: '(DUB)',
			category: 'Hotel'
		},
		{
			label: 'The Shelbourne Hotel Dublin',
			label_name: 'The Shelbourne Hotel Dublin, DUBLIN, IE',
			value: 'The Shelbourne Hotel Dublin, DUBLIN, IE',
			label_code: '(DUB)',
			category: 'Hotel'
		},
		{
			label: 'Panorama Bur Dubai',
			label_name: 'Panorama Bur Dubai, DUBAI, AE',
			value: 'Panorama Bur Dubai, DUBAI, AE',
			label_code: '(DXB)',
			category: 'Hotel'
		},
		{
			label: 'Westbury',
			label_name: 'Westbury, DUBLIN, IE',
			value: 'Westbury, DUBLIN, IE',
			label_code: '(DUB)',
			category: 'Hotel'
		}
	];

	///////
	$.widget('custom.catcomplete', $.ui.autocomplete, {
		_create: function() {
			this._super();
			this.widget().menu('option', 'items', '> :not(.ui-autocomplete-category)');
		},
		_renderMenu: function(ul, items) {
			var that = this,
				currentCategory = '';
			$.each(items, function(index, item) {
				var li;
				if (item.category != currentCategory) {
					var cl = item.category == 'Location' ? 'fa-map-marker' : 'fa-hotel';
					ul.append(
						"<li class='ui-autocomplete-category'>" +
							"<i class='fa " +
							cl +
							"'></i>" +
							item.category +
							'</li>'
					);
					currentCategory = item.category;
				}
				li = that._renderItemData(ul, item);
				if (item.category) {
					li.attr('aria-label', item.category + ' : ' + item.label);
				}
			});
		}
	});

	$('#autocomplete_hotel').catcomplete({
		delay: 0,
		source: demo_listing
	});

	//////////// DEMO AUTOCOMPLATE, can be removed after testing
	var availableTags = [
		'Dubai - UAE',
		'Al Maktoum',
		'Istanbul',
		'London',
		'New York',
		'Qatar Airways',
		'Lufhanza',
		'United airlines',
		'Washington'
	];
	$('#autocomplete').autocomplete({
		source: availableTags
	});

	//////////////////////// passenger select
	$('.passenger-change').click(function(e) {
		$(this).next('.passenger-dropdown').slideToggle('fast');
		e.stopPropagation();
		e.preventDefault();
	});

	$('.passenger-dropdown').click(function(e) {
		e.stopPropagation();
	});

	$('.passenger-dropdown .btn-ok').click(function(e) {
		e.preventDefault();
		$('.passenger-dropdown').hide();
	});

	$('body').click(function() {
		$('.passenger-dropdown').hide();
	});

	//////////////////////// hotel form occupany select
	$('.occupancy-change').click(function(e) {
		$(this).next('.occupancy-dropdown').slideToggle('fast');
		e.stopPropagation();
		e.preventDefault();
	});

	$('.occupancy-dropdown').click(function(e) {
		e.stopPropagation();
	});

	$('.occupancy-dropdown .btn-ok').click(function(e) {
		e.preventDefault();
		$('.occupancy-dropdown').hide();
	});

	$('body').click(function() {
		$('.occupancy-dropdown').hide();
	});

	$('.btn-one-way').click(function() {
		$('.flight-tab-switcher').addClass('hide');
		$('#one-round-block').removeClass('hide');
		$('.date-return-active').hide();
		$('.date-return-empty').show();
		$(this)
			.closest('.form-block')
			.find('.trip-type-hidden')
			.val($(this).children("input[name='trip_type_chk']").val());
		$(this).addClass('active').siblings().removeClass('active');
	});

	$('.btn-round-trip').click(function() {
		$('.flight-tab-switcher').addClass('hide');
		$('#one-round-block').removeClass('hide');
		$('.date-return-empty').hide();
		$('.date-return-active').show();
		$(this)
			.closest('.form-block')
			.find('.trip-type-hidden')
			.val($(this).children("input[name='trip_type_chk']").val());
		$(this).addClass('active').siblings().removeClass('active');
	});

	$('.btn-multy-city').click(function() {
		$('.flight-tab-switcher').addClass('hide');
		$('#multi-block').removeClass('hide');
		$(this)
			.closest('.form-block')
			.find('.trip-type-hidden')
			.val($(this).children("input[name='trip_type_chk']").val());
		$(this).addClass('active').siblings().removeClass('active');
	});

	$('.add-return').click(function() {
		$('.btn-round-trip').click();
	});
	$('.btn-remove-return').click(function() {
		$('.btn-one-way').click();
	});

	// $('.btn-one-way').click(function () {
	//     $('.flight-tab-switcher').addClass('hide');
	//     $('#one-round-block').removeClass('hide');

	//     $('.date-return-active').hide();
	//     $('.date-return-empty').show();

	// });

	// $('.btn-round-trip').click(function () {
	//     $('.flight-tab-switcher').addClass('hide');
	//     $('#one-round-block').removeClass('hide');

	//     $('.date-return-empty').hide();
	//     $('.date-return-active').show();

	// });

	// $('.btn-multy-city').click(function () {
	//     $('.flight-tab-switcher').addClass('hide');
	//     $('#multi-block').removeClass('hide');
	// });

	/////////////////////// timepicker
	if ($('.timepicker').length > 0) {
		// check if element exists
		$('.timepicker').timepicker({
			scrollDefault: 'now',
			showDuration: true
		});
	}

	/////////////////////// spinner
	if ($('.table-flight-calendar').length > 0) {
		// check if element exists
		$('.table-flight-calendar td').hover(
			function() {
				classname = $(this).attr('class').split(' ')[1];
				//alert(classname);
				$(this).parent().siblings().find('.' + classname).addClass('highlight-col');
			},
			function() {
				$('.table-flight-calendar td, .table-flight-calendar th').removeClass('highlight-col');
			}
		);
	}

	// //////////////////////// Scroll top
	var scroll_btn = $("a[href='#top']");
	scroll_btn.hide();
	$(window).scroll(function() {
		if ($(this).scrollTop() > 500) {
			scroll_btn.fadeIn();
		} else {
			scroll_btn.fadeOut();
		}
	});
	scroll_btn.click(function() {
		$('html, body').animate({ scrollTop: 0 }, 'slow');
		return false;
	});

	// external js: isotope.pkgd.js

	// init Isotope
	// var $grid = $('.grid').isotope({
	// 	itemSelector: '.element-item',
	// 	layoutMode: 'fitRows',
	// 	getSortData: {
	// 		name: '.name',
	// 		symbol: '.symbol',
	// 		number: '.number parseInt',
	// 		category: '[data-category]',
	// 		weight: function(itemElem) {
	// 			var weight = $(itemElem).find('.weight').text();
	// 			return parseFloat(weight.replace(/[\(\)]/g, ''));
	// 		}
	// 	}
	// });

	// filter functions
	var filterFns = {
		// show if number is greater than 50
		numberGreaterThan50: function() {
			var number = $(this).find('.number').text();
			return parseInt(number, 10) > 50;
		},
		// show if name ends with -ium
		ium: function() {
			var name = $(this).find('.name').text();
			return name.match(/ium$/);
		}
	};

	// bind filter button click
	$('#filters').on('click', 'button', function() {
		var filterValue = $(this).attr('data-filter');
		// use filterFn if matches value
		filterValue = filterFns[filterValue] || filterValue;
		$grid.isotope({ filter: filterValue });
	});

	// bind sort button click
	$('#sorts').on('click', 'button', function() {
		var sortByValue = $(this).attr('data-sort-by');
		$grid.isotope({ sortBy: sortByValue });
	});

	// change is-checked class on buttons
	$('.button-group').each(function(i, buttonGroup) {
		var $buttonGroup = $(buttonGroup);
		$buttonGroup.on('click', 'button', function() {
			$buttonGroup.find('.is-checked').removeClass('is-checked');
			$(this).addClass('is-checked');
		});
	});
});
// jquery end





$(document).ready(function() {
    $("#room_count").change(function() {
        var new_room_count = parseInt($(this).val());
        if (new_room_count >= 1 && new_room_count <= 4) renderRoomBlock(new_room_count)
    })
})

function countOccupancies() {
    var rooms = 0,
        adults = 0,
        child = 0;
    $("#rooms-repeat-block").find('[id^="room-"]:visible').each(function() {
        var roomId = $(this).attr('id').split('-')[1];
        adults += parseInt($("#adults-" + roomId).val());
        child += parseInt($("#children-" + roomId).val())
    });
    rooms = parseInt($("#room_count").val());
    $("#occ-room-count").text(rooms);
    $("#occ-adult-count").text(adults);
    $("#occ-child-count").text(child);
    $("#checkout-date-txt-block").toggleClass('hide', child === 0)
}

function renderRoomBlock(roomsSelected) {
    var source = $("#room-repeat-template").html(),
        template = Handlebars.compile(source),
        roomsDisplayed = $("#rooms-repeat-block").find('[id^="room-"]:visible').length,
        roomsRendered = $("#rooms-repeat-block").find('[id^="room-"]').length;
    if (roomsSelected > roomsDisplayed) {
        for (var i = 1; i <= roomsSelected; i++) {
            var r = $('#room-' + i);
            if (r.length == 0) {
                var html = template({
                    roomNumber: i
                });
                $('#rooms-repeat-block').append(html);
                roomsRendered += 1
            } else $(r).show()
        }
    } else
        for (var i = ++roomsSelected; i <= roomsRendered; i++) $('#room-' + i).hide();
    countOccupancies()
}

function renderAgeDropdown(that, roomId) {
    var source = $("#child-age-repeat-template").html(),
        template = Handlebars.compile(source),
        childSelected = $(that).val(),
        childDisplayed = $('[id^="child-' + roomId + '-"]:visible').length,
        childRendered = $("#rooms-repeat-block").find('[id^="room-' + roomId + '"]').find('[id^="child-"]').length;
    if (childSelected > childDisplayed) {
        for (var i = 1; i <= childSelected; i++) {
            var ch = $('#child-' + roomId + '-' + i);
            if (ch.length == 0) {
                var html = template({
                    roomNumber: roomId,
                    childNumber: i
                });
                $('#child-age-block-' + roomId).append(html);
                childRendered += 1
            } else $(ch).show()
        }
    } else
        for (var i = ++childSelected; i <= childRendered; i++) $('#child-' + roomId + '-' + i).hide();
    countOccupancies()
}